from pyx import *

canv = canvas.canvas()

e_logo = path.path( path.moveto( 0, 0 ), path.arc( -7, 0, 7, 0, 270 ))
canv.stroke(e_logo, [style.linewidth(2.2), color.rgb.blue])

sin = path.curve( -10.0, -0.5, -6.5 , 5, -4.5, -6, -1.5,0 )
canv.stroke(sin, [style.linewidth(2), color.cmyk.RedOrange])

w = path.path( path.moveto( -17, -12 ), path.lineto( -17, -15 ), path.curveto( -16, -15, -15, -14, -15, -12 ), path.lineto( -15, -15), path.curveto( -14, -15, -13, -14, -13, -12 ))
canv.stroke(w, [style.linewidth(0.6), color.rgb.blue])

e = path.path( path.moveto( -8.3, -13.2 ), path.arc( -10.3, -13.2, 2, 0, 270 ))
canv.stroke(e, [style.linewidth(0.6), color.rgb.blue])

sin_maly = path.curve( -10.7, -13.2, -9.8, -12, -9.6, -14.4, -8.7, -13.2 )
canv.stroke(sin_maly, [style.linewidth(0.5), color.cmyk.RedOrange])

ee = path.path( path.moveto( -5.9, -13.5 ), path.arc( -5.7, -13.5, 1.7, 0, 270 ))
canv.stroke(ee, [style.linewidth(0.6), color.rgb.blue])

i = path.line( -3.2, -15, -3.2, -11.8 )
canv.stroke(i, [style.linewidth(0.6), color.rgb.blue])

a = path.path( path.moveto( 1.2, -15.2 ), path.arc( -0.3, -13.7, 1.5, 0, 270 ))
canv.stroke(a, [style.linewidth(0.6), color.rgb.blue])

canv.writePDFfile()