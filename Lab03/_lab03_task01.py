from pyx import *

canv = canvas.canvas()
circle = path.circle(0, 0, 2)
line = path.line(-1.5,-2, -1.5, 2)
canv.stroke(circle, [style.linewidth.Thick])
canv.stroke(line, [style.linewidth.Thick])
isects_circle, isects_line = circle.intersect(line)


for isect in isects_circle:
    isectx, isecty = circle.at(isect)
    canv.stroke(path.line(0, 0, isectx, isecty), [style.linewidth.Thick])
    arc1, arc2 = circle.split(isects_circle)
    if arc1.arclen() < arc2.arclen():
        arc = arc1
    else:
        arc = arc2
    isects_line.sort()
    line1, line2, line3 = line.split(isects_line)
    segment = (path.line(0,0, isectx, isecty) ) << line2

canv.fill(segment, [color.gray(0.5)])
canv.writePDFfile()