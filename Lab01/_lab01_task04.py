# python3 _lab01_task04.py 212.191.99.68 27

import sys

IP = sys.argv[1]
adresIP = ''.join([bin(int(x)+256)[3:] for x in IP.split('.')])
liczbaBitowMaski = int(sys.argv[2])

mask = []

for i in range(0, liczbaBitowMaski):
    mask.append(1)

for i in range(liczbaBitowMaski, 32):
    mask.append(0)

mask = ''.join(str(e) for e in mask)

def printMask(maska):
    for i in range(8, 32, 8):
        maska.insert(i, '.')
    array = ''.join(str(e) for e in maska)
    print(array)

def ipANDmask(adresIP, maska):
    a = int(adresIP, 2)
    b = int(maska, 2)

    return bin(a & b)

def maskNOTmask(mask):

    for i in range(0,32):
        if mask[i] == 1:
            mask[i] = 0
        else:
            mask[i] = 1

    return mask

def splitIP(IP):
    arrayIP = [IP[i:i+8] for i in range(0, len(IP), 8)]
    return arrayIP

def decMask(IPaddress):
    binMask = splitIP(IPaddress)
    decMask = ''
    for i in binMask:
        temp = int(i,2)
        if i == binMask[3]:
            decMask += str(temp)
        else:
            decMask += str(temp) + "."

    return decMask

print("")
print("Zakładając że maska = " + ".".join(splitIP(mask)))

#IP and mask
toPrint= ipANDmask(adresIP, mask)[2:]

print("Adres sieci - uzyskujemy poprzez IP AND maska")
print("(" + "" + decMask(toPrint) + ", " + ".".join(splitIP(toPrint)) + ")")

#(IP and mask) +1
toPrint= bin( int(toPrint, 2) + int('1',2))[2:]
print("")
print("Adres pierwszego hosta - uzyskujemy poprzez (IP AND maska) + 1")
print("(" + "" + decMask(toPrint) + ", " + ".".join(splitIP(toPrint)) + ")")

#IP OR (NOT maska)
maskNOT = mask
maskNOT= maskNOTmask([int(s) for s in maskNOT])
maskNOT = ''.join(str(x) for x in maskNOT)

toPrint = bin(int(toPrint,2) | int(maskNOT,2))[2:]

print("")
print("Adres rozgłoszeniowy - uzyskujemy poprzez IP OR (NOT maska)")
print("(" + "" + decMask(toPrint) + ", " + ".".join(splitIP(toPrint)) + ")")


#IP OR (NOT maska) -1
toPrint= bin( int(toPrint, 2) - int('1',2))[2:]

print("")
print("Adres ostatniego hosta - uzyskujemy poprzez IP OR (NOT maska) -1")
print("(" + "" + decMask(toPrint) + ", " + ".".join(splitIP(toPrint)) + ")")