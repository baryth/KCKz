months = ["sty", "lut", "mar", "kwi", "maj", "cze", "lip", "sie", "wrz", "paź", "lis", "gru"]

m1 = input('Podaj nazwe miesiaca (OD): ')
m2 = input('Podaj nazwe miesiaca (DO): ')

m1 = m1[:3]
m2 = m2[:3]

m1num = months.index(m1)+1
m2num = months.index(m2)+1

if(m1num == m2num):
    print(12)
else:
    if((m2num-m1num) < 0):
        print(12 + (m2num-m1num))
    else:
        print(m2num-m1num)
